async function fetchLatestReleaseTag() {
    const url = `https://gitlab.com/api/v4/projects/55558123/releases`;
    try {
        const response = await fetch(url, {
            headers: {
                "Content-Type": "application/json",
            },
        });
        const releases = await response.json();

        if (releases && releases.length > 0) {
            console.log("Latest release tag:", releases[0].tag_name);
            return releases[0].tag_name; // Assuming the first release is the latest
        } else {
            console.log("No releases found.");
            return null;
        }
    } catch (error) {
        console.error("Error fetching latest release tag:", error);
        return null;
    }
}
 
export const load = (async () => {
    let latestReleaseTag = await fetchLatestReleaseTag();
    console.log(latestReleaseTag)
    return latestReleaseTag //? { body: latestReleaseTag } : { status: 404 };
}) 